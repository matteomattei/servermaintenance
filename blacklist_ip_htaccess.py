#!/usr/bin/env python

"""
This script creates a .htaccess file with a full list of dangerous IP address.
Every range of dangerous addresses is properly denied.
The parsed ranges are taken from www.wizcrafts.net
"""

import subprocess 
import re

DEBUG = True
FILE  = '.htaccess'
pages={
	'chinese':'http://www.wizcrafts.net/chinese-blocklist.html',
	'russians':'http://www.wizcrafts.net/russian-blocklist.html',
	'nigerians':'http://www.wizcrafts.net/nigerian-blocklist.html',
	'southamerica':'http://www.wizcrafts.net/lacnic-blocklist.html',
	'exploited':'http://www.wizcrafts.net/exploited-servers-blocklist.html',
}

f = open(FILE,'w')
f.write('<Files *>\norder deny,allow\n\n')
for item in pages:
	url = pages[item]
	if DEBUG: print('Downloading '+item)
	f.write('# '+item+':')
	page = subprocess.check_output(['wget','-q','-O','-',url])
	lines = page.split(b'\n')
	pre = ''
	record = False
	for l in lines:
		if b'This blocklist was last updated on' in l:
			update = l.decode().strip().replace('<p><b>','').replace('</b></p>','')
			f.write(' '+update+'\n')
			continue
		if b'<pre>' in l:
			record=True
			continue
		if b'</pre>' in l:
			record = False
			continue
		if record:
			l = l.strip()
			if l.startswith(b'deny from') or l.startswith(b'allow from'):
				if b'all' in l: continue
				f.write(l.decode()+'\n')
	f.write('\n')
f.write('</Files>\n\n<Files .htaccess>\ndeny from all\n</Files>\n')
f.close()	
